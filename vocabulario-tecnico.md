# Vocabulário

Reiniciar o computador: Não se reinicia o computador, se reinicia o sistema, computador liga e desliga

Formatei o HD: HD não se formata, se formata o sistema de arquivos das partições do HD

Gravar um arquivo no hd: você grava 0’s e 1’s (informação) que podem ser interpretados como um tipo arquivo

Apagar arquivo: zerar o bloco onde se encontra a representação e informação do arquivo

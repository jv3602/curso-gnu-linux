# Redes

## TCP/IP

IP: Internet Protocol

IP number: Número do IP

IP: 32 bits ou 4 bytes (na v4) divididos em 4 octetos (os 4 bytes)
```
0000000 | 0000000 | 0000000 | 0000000
   0         0         0         0
  255       255       255       255
```
IPs reservados: não roteáveis, privados

ex: 192.168.0.1

## Classes:

### Classe A:

0.0.0.0 -> 126.255.255.255

IPs Reservados: 10.0.0.0 -> 10.255.255.255

### Classe B:

128.0.0.0 -> 191.255.255.255

IPs Reservados: 172.16.0.0 -> 176.31.255.255

### Classe C: 

192.0.0.0 -> 223.255.255.255

IPs Reservados: 192.168.0.0 -> 192.168.255.255

### Classe D:

224.0.0.0 -> 239.255.255.255

### Classe E: 

240.0.0.0 -> 255.255.255.255

## Máscara

Como sabemos se um IP está na mesma rede ou não? Depende da máscara da rede

24/8: 24 bits para representar redes, 8 para hosts

por exemplo:

```
192.168.0 | .1
192.168.0.0 -> 192.168.0.255
```

Nos IPs, o primeiro IP (como 192.168.0.0 nessa rede) é utilizado para representar a rede e o último IP (como o 192.168.0.255 nessa rede) é utilizado como IP broadcast, que é o endereço de IP que se comunica com todas as subredes.

Então neste exemplo, totalizá-se 256 IPs e 254 hosts disponíveis nessa rede

Se a máscara fosse 25/7, o IP teria 2 subredes, que iriam de:

```
1) 192.168.0.0 -> 192.168.0.127
2) 192.168.0.128 -> 192.168.255
```

Cada subrede teria 128 IPs e a capacidade de 126 hosts.

Já com a máscara 23/9, teríamos:

```
10.0.0.0 -> 10.0.1.255
```

Totalizando 512 IPs e a capacidade de 510 hosts.

### Formas de Representar Máscara:

#### CIDR:

24/8: 24 bits para rede e 8 para hosts

#### Decimal:

```
11111111 11111111 11111111 00000000
   255      255      255       0
```
255.255.255.0: 24 bits para rede (os que tão em 1) e 8 para hosts (os que tão em 0)

## Gateway

Gateway é a maquina que liga a sua rede a outras redes, por exemplo, um roteador, um moldem, outro servidor. É comum que o gateway seja o primeiro ou o último IP válido de uma rede.

Exemplo: 

Máscara: /24

192.168.0.0 -> 192.168.0.255

Estão na mesma rede, não necessita de gateway

192.168.0.0 -> 1.1.1.1

Não estão na mesma rede, necessita de gateway


## DHCP

Dynamic Host Configuration Protocol (DHCP) serve para obtermos o IP automaticamente.
Para isso ocorrer, primeiro o cliente DHCP faz uma requisição DHCP pelo protocolo ethernet, que é respondida por um servidor DHCP (que está em outra máquina na mesma rede) informando o IP, a máscara, o Gateway eo DNS da rede.

## Cálculo de Subredes

O IP só é dividido em subredes de potências de 2, então ele não pode ser dividido em 3 ou 6 subredes, só 2, 4, 8 e assim em diante.

Ex1: 192.168.1.0/24

Se mudarmos a máscara desse IP para /25 (em decimal: 255.255.255.128), o IP se dividiria em 2 subredes:

```
192.168.1.0 -> 192.168.1.127

192.168.1.128 -> 192.168.1.255
```

Ex2: 172.16.0.0/16

Se mudarmos a máscara para /17 (em decimal: 255.255.128.0), o IP se dividiria em 2 subredes:

```
172.16.0.0 -> 172.16.127.255
172.16.128.0 -> 172.16.255.255
```

Ex3: 10.0.0.0/29

```
10.0.0.0 -> 10.0.0.7
10.0.0.8 -> 10.0.0.15
10.0.0.16 -> 10.0.0.23
e assim por diante
```

Para descobrirmos quantas subredes serão formadas, colocamos  os bits usados para representar redes (5) como expoente de 2:

2⁵ = 32 subredes

Para descobrirmos quantos IPs teremos em cada subrede, colocamos os bits usados para representar hosts (3) como expoente de 2

2³ = 8 IPs por subrede

Potência de 2 mais próximo e acima da quantidade.

## Curiosidades

### Versões do IP e... onde está o IPv5?

  Da 1ª até a 3ª versão do IP, ele não era algo distinguido, ele era integrado ao protocolo TCP. Após perceberem que não era uma boa ideia juntar o protocolo de host e de internet juntos, separaram o IP e o TCP em 2 protocolos diferentes, começando da 4ª versão (já que o TCP original já estava na 3ª), daí surgiu o famigerado IPv4, que usamos até hoje. O IPv4 tem 32 bits, dando  4.294.967.296 de endereços diferentes, a primeira vista parece muito, mas quando você pensa no número de dispositivos que usam internet, você percebe que esse número não tem condições de dar um IP único dispositivo, e este é seu maior problema.	
  O IPv5 chegou a ganhar um rascunho, com foco em streaming de mídias, como música e vídeo, mas não chegou a sair do papel, mas para não dar confusão entre os nomes, acabaram só pulando o 5. Um grande problema desse protocolo é que ele não consertou o maior defeito do seu antepassado, o número limitado de endereços.
  Já o IPv6 chegou com grandes atualizações em relação ao IPv4, com 128bits invés de 32, totalizando 2¹²⁸ endereços de IP, além de ser mais rápido, ter IPsec incluido, que aumenta a segurança, e possuir uma autoconfiguração (SLAAC).
  Só não vimos o IPv6 sendo utilizado por todos ainda por falta de investimento das ISPs, mas num futuro próximo, o IPv6 será o padrão da Internet.

### Unidades de data em redes

#### Pacote

É a unidade de dados de um protocolo da camada de rede. Cada pacote contém uma header com o endereço de IP de origem e destino, entre outras informações.

#### Fragmentos

Quando um pacote é enviado e ele tem o tamanho maior que o MTU (Maximum Transmitted Unit), ele é fragmentado em vários fragmentos que são reagrupados na próxima layer. Se o tamanho do pacote for maior que o MTU e a flag DF (Don't Fragment) estiver setada como 1, o pacote é descartado.

#### Frame

É a unidade de dados de um protocolo da camada de enlace de dados (data link). Em sua header contém o endereço MAC de origem e destino.

#### Datagram

É a unidade de dados usada no protocolo UDP. Em sua header contém o endereço IP de origem e destino e seus dados

# Utilitários

## Corrigindo erro módulo nvidia

um dia eu completo

## Jeito fácil de dar mount na hora de instalar o debian no pendrive
```
mount /dev/sdX /mnt
arch-chroot /mnt
```
## Limpando Sistema
```
dpkg -l | grep ^rc
apt purge ~c
```

## Perdemos o GRUB!!

Irei usar de exemplo como se o Debian estivesse no hd0,gpt2

Reconhecendo o que está armazenado em cada dispositivo, para sabermos qual é o dispositivo que queremos:
```
ls (hd0,gpt2) (mostra o tipo de sistema de arquivos)
ls (hd0,gpt2)/ (lista os arquivos da partição X do hd X)
```
Após achar o dispositivo e partição onde fica o gnu, localizamos o /boot
```
ls (hd0,gpt2)/boot
ls (hd0,gpt2)/boot/grub
```
Depois carregamos o arquivo do grub para podermos bootar:
```
configfile (hdX,gptX)/boot/grub/grub.cfg
```
Após bootarmos a máquina, rodamos o seguinte comando para gravarmos na memória da UEFI a entrada Debian
```
grub-install /dev/sdX
update-grub
```
E feito!

## Instalando o grub

```
apt install grub
grub-install /dev/sdX
grub-mkconfig -o /boot/grub/grub.cfg
```

## Consertando "could not resolve"

Primeiros precisamos checar 3 coisas:

Tem IP?
Tem conectividade?
Qual DNS está usando?

Para verificarmos isso, usaremos respectivamente 3 comandos:


```
ip -c a
ping google.com
cat /etc/resolv.conf

```
Caso o IP esteja DOWN, usamos:

```
ip link set dev [dev-name] up - Para setar UP
ip a add [IP]/[Mask] dev [dev-name] - Para adicionar o ip à maquina
ip -c a - Para mostrar seu IP Adress
```

Depois disso iremos ver a rota, e, se não houver nenhuma, adicionaremos uma:

```
ip -c r
ip r add default via 192.168.122.1 dev enp1s0
ip -c r
```

E por fim, se não houver DNS, use este comando para configurá-la: 

```
dhconfig
```

ou configure manualmente manualmente:

```
echo nameserver 8.8.8.8 >/etc/resolv.conf 
```

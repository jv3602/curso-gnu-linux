# man universal

## Pacotes úteis:
```
rm: deletar arquivo
mv: mover arquivo
cp: copiar arquivo
pwd: mostra o diretório atual
cd: muda o diretório / cd - : volta para o diretório anterior
grep: filtra | *$ (terminando em *)
man: manual de algum pacote
clear: limpar terminal
open: abre arquivo
cat: mostra o conteúdo de um arquivo (muito útil para .txt)
head: mostra as 10 primeiras linhas
tail: mostra as 10 últimas linhas
tee: redireciona o comando para 2 lugares
file: mostra qual o tipo de arquivo
pacmd: reconfigura o dispositivo de som (pa = PulseAudio)
pactl: controla o dispositivo de som atual
sort: organiza os arquivos de um diretório por ordem dependendo do parâmetro.
xdg-open: abre um arquivo com o aplicativo padrão
md5sum: mostra o hash md5 de um arquivo
less: pagina um arquivo
wc: word count (conta palavras)
mkfs.*: cria um sistema de arquivo (filesystem)
mount:
umount:
eject: ejetar disco de armazenamento
fsck -N: vê o sistema de arquivos de uma partição
uname -a: Kernel que está rodando
lsusb -tv: mostra módulos utilizados pelos dispositivos
lsmod: listar módulos carregados
modinfo: info do módulo
modprobe: carrega um módulo e seus dependentes
insmod: carrega um módulo desde que não haja módulos dependentes
rmmod: descarrega um módulo desde que não haja módulos dependentes
lspci -nnkd::*: lista dispositivos na entrada pci (0200 para placa de rede)
dpkg -l: mostrar aplicativos instalados
chmod: muda as permissões do arquivo
fdisk: administra particionamento
fdisk -l: mostra detalhes do disco (partições, tamanhos de partições, hd ou ssd)
adduser: adiciona um usuário
addgroup: adiciona um grupo
userdel: deleta um usuário
groupdel: deleta um grupo
ss -ntpl: ver portas que estão em escuta.
virt-manager: administrador de maquinas virtuais
```

## Git
```
git clone: clona um repositório git
git pull: sincroniza um repositório git
git status: mostra o status do repositório git
git push
```
## Diretórios úteis:
```
Módulos: /lib/modules/
Usuários: /etc/passwd
Dispositivos: /dev/
Repositórios: /etc/apt/source.list
Nome do computador: /etc/hostname
```
## VI:

### Atalhos:
```
g: começo do arquivo
shift + g: final do arquivo
w: próxima palavra
$: final da linha
^: começo da linha
J: sobe linha
K: desce linha
d + d: deleta linha
[número de linha] + d + [direção]: deleta mais de uma linha
p: paste (cola)
u: undo (desfaz)
[número] + u: desfaz as últimas [número] ações
y: copiar
ctrl + r: redo (refaz)
/[termo]: procura um termo no arquivo
n: próxima ocorrência do termo
shift n: ocorrência anterior do termo
```
### Comandos:
```
:q - saí do arquivo
:w - salva o arquivo
:x - salva o arquivo e sai
```
## Glossário:
```
~ : /home/[usuário-corrente]
. :diretório corrente
.. : diretório anterior
* : se refere a todos
> : sobrescrever
>> : adicionar
```

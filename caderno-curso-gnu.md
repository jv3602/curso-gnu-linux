# Caderno

## Definições

Arquitetura: define o conjunto de instruções  e o tanto de bits que o processador vai usar.

Opcodes: Instruções processadas pelo processador.

Compilar: converter uma linguagem de programação em código binário (opcodes).

Frequência do processador (em hz): quantidade de instruções a cada segundo.

Barramentos: SATA, USB, PCi.

Programa: Conjunto de instruções em uma linguagem de programação que o computador pode executar ou interpretar.

Instalar SO: copiar uma porção de arquivos e preparar para boot.

Kernel: programa central onde há as funções essenciais de um computador.

Módulo: trecho de código que se comunica com o kernel.

API: intermediário entre aplicação e hardware (feito pelo kernel).

Inode - onde são guardados os metadados, as informações sobre o arquivo. Quando "apagamos" um arquivo, na verdade apagamos só o inode, por isso é tão rápido.

Kernel tem um módulo para o sistema de arquivos que entende os inodes (ficha do arquivo), onde contém os metadados do arquivo, como permissões de usuários/grupos (quem pode ler, alterar, executar o arquivo)

Principais diretórios:
/ → barra ou raiz
/proc → processos
/dev → dispositivos
/boot → kernel, initrd
/bin → binários (E)
/sbin → binários adm
/lib → bibliotecas
/etc → configuração 
/media → removíveis
/mnt → temporários
/root → usuário root
/home → usuários
/tmp → temporário
/var → variável
/usr/share → independente
/usr/share/doc → documentação
/usr/share/man → manuais
/var/log -> log


### Classes:
uid: dono do arquivo
gid: grupo dono do arquivo
outros: demais usuários
all: todos os usuários
ugoa

``
-rwxrwxrwx
``
```
“-” - tipo de arquivo: d (diretório), b (blocos), - (arquivo regular)
resto - em ordem, permissão de ler (r), “escrever”, que seria alterar o arquivo ou excluí-lo (w) e executar (x), do uid, gid dos outros
suid bit (S): executa o arquivo com as permissões do dono
```
As permissões também podem ser apresentada em octogonais

-rwxr-xr--
```
r w x   r - x   r - - 
4 2 1   4 2 1    4 2 1
  7       5        4
```
Permissão do arquivo em octagonal: 754

Diretórios podem ter a permissão de ser “executados”, que te dá a permissão de acessá-los (?)

A usuária root quebra o conceito de permissão, podendo escrever em tudo, ler tudo e executar tudo, podendo consequentemente, fuder tudo.
```
chmod: muda as permissões do arquivo
chmod +r teste | dá permissão para todos de ler o arquivo
chmod o-r teste| tira a permissão dos outros de ler o arquivo

chown: muda o dono dono do arquivo
chown jv: teste | muda o dono e o grupo para jv
chown jv:root | muda o dono para jv e o grupo para root
```

## Acesso Remoto


Protocolo: A “linguagem que vai ser falada no servidor”, por exemplo ssh, vnc.

Conectividade: TCP/UDP

Portas: Diferencia a informação que vai ser recebida.

CGnat: IP “compartilhado”
```
Túnel: 

Definição:
ssh -R 3000:localhost:22 jv@x.x.x.x
ssh: protocolo
-R: remoto
3000: porta que será aberta no servidor
localhost: cliente
22: porta que será utilizada pelo cliente (22 porta padrão do ssh)
jv: usuário no servidor
x.x.x.x: ip do servidor
```
## Virtualização
